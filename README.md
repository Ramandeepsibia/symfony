# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Symfony cheat sheet
 ##
**Run an existing symfony project** 
composer install

**Create a new symfony project** 
composer create-project symfony/framework-standard-edition jobeetSymfony

**Run a symfony project**
php bin/console server:run

**Create a database as described in parameters.yml and config.yml**
php bin/console doctrine:database:create

**Create columns and table in a database after creating an Entity Class**
php bin/console doctrine:schema:update --force

**Update table schema**
php bin/console doctrine:schema:update --dump-sql

**Create getters and setters**
php bin/console doctrine:generate:entities AppBundle:Genus

**Changing type of a column in a table**
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact